import os.path
from flask import Flask, render_template, url_for, request, redirect, flash
from flask_sqlalchemy import SQLAlchemy
from fantlab import fantlab_init_search, search_simmilars, work_search
import urllib.request
import urllib.parse
from os.path import exists
from flask_login import UserMixin, login_user, login_required, LoginManager, logout_user
from werkzeug.security import check_password_hash, generate_password_hash

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///main.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
manager = LoginManager(app)
app.secret_key = 'asdoiou89734yhrfbiv7t!@RDmvfdiepmuit5'
db = SQLAlchemy(app)


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(128), nullable=False, unique=True)
    password = db.Column(db.String(256), nullable=False)

class Books(db.Model):
    work_id = db.Column(db.Integer, primary_key=True)
    all_autor_rusname = db.Column(db.String(100), nullable=False)
    autor1_id = db.Column(db.Integer, nullable=False)
    fullname = db.Column(db.String(100), nullable=False)
    midmark = db.Column(db.Float, nullable=False)
    description = db.Column(db.Text, nullable=False)
    img_path = db.Column(db.String(100), nullable=False)

    def __repr__(self):
        return '<Books %r>' % self.work_id


@manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

@app.route('/<int:work_id>')
def detail(work_id):
    full_info = Books.query.get(work_id)
    return render_template('detail.html', full_info=full_info)


@app.route('/login', methods=['POST', 'GET'])
def login():
    login = request.form.get('login')
    password = request.form.get('password')

    if login and password:
        user = User.query.filter_by(login=login).first()

        if user and check_password_hash(user.password, password):
            login_user(user)
            next_page = request.args.get('next')
            return redirect(url_for('index'))
        else:
            flash('Wrong login or password')
    else:
        flash('Please fill login and password')
    return render_template('login.html')

@app.route('/registration', methods=['POST', 'GET'])
def registration():
    login = request.form.get('login')
    password = request.form.get('password')
    password2 = request.form.get('password2')

    if request.method == 'POST':
        if not (login or password or password2):
            flash('Please fill all fields!')
        elif password != password2:
            flash('Passwords are not musch')
        else:
            hash_pwd = generate_password_hash(password)
            new_user = User(login=login, password=hash_pwd)
            db.session.add(new_user)
            db.session.commit()

            return redirect(url_for('login'))
    return render_template('registration.html')

@app.after_request
def redirect_to_singin(response):
    if response.status_code == 401:
        return redirect(url_for('login') + '?next' + request.url)
    return response


@app.route('/logout', methods=['POST', 'GET'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('registration'))

@app.route('/all_result')
@login_required
def result():
    book = Books.query.order_by(Books.work_id).all()
    return render_template('all_result.html', book=book)


def take_id_from_db():
    book_id = Books.query.with_entities(Books.work_id).all()
    book_id = [int(i[0]) for i in book_id]
    return book_id


def save_to_db(work_id, all_autor_rusname, autor1_id, fullname, midmark, description, img_path):
    book = Books(work_id=work_id, all_autor_rusname=all_autor_rusname, autor1_id=autor1_id,
                 fullname=fullname, midmark=midmark, description=description, img_path=img_path)
    db.session.add(book)
    db.session.commit()


@app.route("/", methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        work_name = request.form['work_name']
        global result
        result = fantlab_init_search(work_name)
        for grow in result:
            image, description = work_search(grow['work_id'])
            grow['image'] = image
            grow['description'] = description
        return redirect('/search_result')
    else:
        return render_template('index.html')


@app.route('/simmilars.html', methods=['POST', 'GET'])
def take_simmilars():
    if request.method == 'POST' and request.get_data(as_text=True).split('=')[-1] == 'Simmilars':
        work_id = request.get_data(as_text=True).split('=')[0]
        global simmilars
        simmilars = search_simmilars(work_id)
        for one_item in simmilars:
            if one_item['image'] and not exists(f'static/img/{str(one_item["id"])}.jpeg'):
                img_path = f'static/img/{str(one_item["id"])}.jpeg'
                if not os.path.exists(img_path):
                    img_path = f'static/img/{one_item["id"]}.jpeg'
                    urllib.request.urlretrieve('https:' + one_item['image'], img_path)
        return render_template('/simmilars.html', simmilars=simmilars)
    if request.method == 'POST' and request.get_data(as_text=True).split('=')[-1] == 'Save':
        tmp = dict()
        work_id = request.get_data(as_text=True).split('=')[0]
        for item in simmilars:
            if int(work_id) == int(item['id']):
                tmp = item
        print(tmp)
        if tmp['image']:
            img_path = f'static/img/{str(tmp["id"])}.jpeg'
        else:
            img_path = f'static/img/default.jpeg'
        save_to_db(work_id=int(tmp['id']), all_autor_rusname=tmp['creators']['authors'][0]['name'], autor1_id=int(tmp['creators']['authors'][0]['id']), fullname=tmp['name'],
                   midmark=float(tmp['stat']['rating']), description=tmp['description'], img_path=img_path)
        return render_template('/simmilars.html', simmilars=simmilars)
    else:
        return render_template('/simmilars.html', simmilars=simmilars)


@app.route('/search_result', methods=['POST', 'GET'])
def search_result():
    if request.method == 'POST':
        for_save = dict()
        work_id = request.get_data(as_text=True).split('=')[0]
        for i in result:
            if str(i['work_id']) == str(work_id):
                for_save = i
        book_id = take_id_from_db()
        if work_id not in book_id:
            save_to_db(work_id=int(for_save['work_id']), all_autor_rusname=for_save['all_autor_rusname'], autor1_id=int(
                for_save['autor1_id']), fullname=for_save['fullname'], midmark=float(for_save['midmark'][0]), description=for_save['description'], img_path=for_save['image'])
        return render_template('search_result.html', result=result)
    else:
        for i in result:
            if i['image']:
                img_path = f'static/img/{str(i["work_id"])}.jpeg'
                if not os.path.exists(img_path):
                    img_path = f'static/img/{i["work_id"]}.jpeg'
                    urllib.request.urlretrieve('https:' + i['image'], img_path)
        return render_template('search_result.html', result=result)


if __name__ == '__main__':
    app.run(debug=True)
