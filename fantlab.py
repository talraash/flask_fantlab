import requests

# GET /search-ids?a={autor_ids}&w={work_ids}&e={edition_ids}


def fantlab_init_search(work_name):
    res = requests.get(
        f'https://api.fantlab.ru/search-works?q={work_name}&page=1&onlymatches=1')
    res = res.json()
    return res[0:6]


def search_simmilars(work_id):
    simmilars = requests.get(f'https://api.fantlab.ru/work/{work_id}/similars')
    simmilars = simmilars.json()
    return simmilars[0:6]


def work_search(work_id):
    work = requests.get(f'https://api.fantlab.ru/search-ids?w={work_id}')
    image = work.json()['works'][0]['image']
    description = work.json()['works'][0]['description']
    return [image, description]
